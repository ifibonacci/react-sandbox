import React, { useEffect, useState } from "react";
import { DatePickerField } from "./DatePickerField";
import "react-datepicker/dist/react-datepicker.css";
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';
import axiosInstance from '../Services/ApiCallService';
import { ThreeCircles } from  'react-loader-spinner';

const schema = Yup.object().shape({
  symbol: Yup.string().required('Required'),
  email: Yup.string().email('Invalid email').required('Required'),
  startDate: Yup.date().max(Yup.ref('endDate')).required('Required'),
  endDate: Yup.date().min(Yup.ref('startDate')).required('Required'),
});

function SymbolForm() {
  const [companies, setCompanies] = useState([]);
  // const [payments, setPayments] = useState([]);

  const setPayments = async function(payments) {
    console.log(payments)
  }

  useEffect(() => {
    getListOfSymbols();
  }, []);

  const getListOfSymbols = function (ignore) {
    axiosInstance.get('/list-of-symbols')
      .then((response) => {
        setCompanies(response.data)
      }).catch((messages) =>{
      console.error(messages)
    });
  }
  //
  const errorMaker = function (errorMessages) {
    const listErrors = {
      symbol: [],
      email: [],
      startDate: [],
      endDate: [],
    };

    errorMessages.map((err) => {
      if (!err.hasOwnProperty('propertyPath') || err[ 'propertyPath' ] === 'prices') {
        return listErrors.symbol.push(err[ 'message' ]);
      } else {
        return listErrors[ err[ 'propertyPath' ] ].push(err[ 'message' ]);
      }
    })

    return listErrors;
  }

  const submitForm = async function (values, actions) {
    // await actions.setFieldValue('loaderStatus', true);

    try {
      const response = await axiosInstance.post('/company-symbol-range',{
        startDate: values.startDate.toISOString().split('T')[0],
        endDate: values.startDate.toISOString().split('T')[0],
        symbol: values.symbol,
        email: values.email,
      });
      await setPayments(response.data);
    } catch (rej) {
      let data = rej.response.data;
      if (data.hasOwnProperty('violations')) {
        let er = errorMaker(data.violations);
        await actions.setErrors(er);
      } else {
        let er = errorMaker([{propertyPath: 'symbol', message: data.error}])
        await actions.setErrors(er);
      }
    }
// await actions.setFieldValue('loaderStatus', false);
  }

  return (
    <>
    <Formik
      validationSchema={schema}
      enableReinitialize={true}
      onSubmit={submitForm}
      initialValues={{
        symbol: 'GOOG',
        email: 'sdfsd@asd.re',
        startDate: new Date(),
        endDate: new Date(),
        companies: [],
        loaderStatus: false,
        payments: []
      }}
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        touched,
        isValid,
        errors,
      }) => (
      <Form
        noValidate
        autoComplete="off"
        onSubmit={handleSubmit}
      >
        <div className="mb-3">
          <label className="form-label">Company symbol</label>
          <select
            className="form-select"
            name="symbol"
            disabled={values.loaderStatus}
            value={values.symbol}
            onChange={handleChange}
            onBlur={handleBlur}
            style={{ display: "block" }}
          >
            {companies.map(company => (
              (company.Symbol && <option key={company.Symbol} value={company.Symbol} label={company["Company Name"]}>{company["Company" +
              " Name"]}</option>)
            ))}
          </select>
          {errors.symbol && touched.symbol ? (
            <div className="danger">{errors.symbol}</div>
          ) : null}
        </div>
        <div className="mb-3">
          <label className="form-label">Start date</label>
          <DatePickerField
            name="startDate"
            disabled={values.loaderStatus}
            dateFormat="yyyy-MM-dd"
            className="form-control"
            maxDate={new Date()}
          />
          {errors.startDate && touched.startDate ? (
            <div className="danger">{errors.startDate}</div>
          ) : null}
        </div>
        <div className="mb-3">
          <label className="form-label">End date</label>
          <DatePickerField
            name="endDate"
            disabled={values.loaderStatus}
            className="form-control"
            dateFormat="yyyy-MM-dd"
            maxDate={new Date()}
            minDate={new Date(values.startDate)}
          />
          {errors.endDate && touched.endDate ? (
            <div className="danger">{errors.endDate}</div>
          ) : null}
        </div>
        <div className="mb-3">
          <label className="form-label">Email</label>
          <Field
            type="email"
            disabled={values.loaderStatus}
            name="email"
            className="form-control"
            placeholder="Email address"
            onChange={handleChange}
            value={values.email}
          />
          {errors.email && touched.email ? (
            <div className="danger">{errors.email}</div>
          ) : null}
        </div>
        <div>
          { !values.loaderStatus &&(<button type="submit" className="btn btn-primary">Submit</button>)}
          <ThreeCircles
            height="100"
            width="100"
            color="#4fa94d"
            wrapperStyle={{}}
            wrapperClass=""
            visible={values.loaderStatus}
            ariaLabel="three-circles-rotating"
            outerCircleColor=""
            innerCircleColor=""
            middleCircleColor=""
          />
        </div>
      </Form>
      )}
      </Formik>
    </>
  );
}

export default SymbolForm;

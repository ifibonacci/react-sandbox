import React, { useEffect, useState } from "react";
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import axiosInstance from '../Services/ApiCallService';
import { ThreeCircles } from  'react-loader-spinner';
import HistoricalQuotes from './HistoricalQuotes'

function MyForm() {
  let initialState = {
    symbol: 'GOOG',
    email: 'asdas@sadf.re'
  }
  const [formData, setFormData] = useState(initialState)
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [companies, setCompanies] = useState([])
  const [loaderStatus, setLoaderStatus] = useState(false)
  const [response, setResponse] = useState([])

  const onChangeHandler = (event) => {
    const {name, value} = event
    setFormData((prev) => {

      return {...prev, [name]: value}
    })
  }
  const onChangeDate = (dates) => {
    const [start, end] = dates;
    setStartDate(start);
    setEndDate(end);
  };

  useEffect(() => {
    getListOfSymbols();
  }, []);

  const getListOfSymbols = function (ignore) {
    axiosInstance.get('/list-of-symbols')
      .then((response) => {
        setCompanies(response.data)
      }).catch((messages) =>{
      console.error(messages)
    });
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    setLoaderStatus(true);
    setResponse([]);

    // Handle validations
    axiosInstance.post('/company-symbol-range',{
      startDate: startDate.toISOString().split('T')[0],
      endDate: endDate.toISOString().split('T')[0],
      symbol: formData.symbol,
      email: formData.email,
    }). then((response) => {
      setLoaderStatus(false);
      setResponse(response.data)
    }). catch(function (error) {
      setLoaderStatus(false);
      console.log(error)
    })
  }

  return (
    <>
      <div className="row">
        <form
          onSubmit={handleSubmit}
          noValidate
          autoComplete="off"
        >
          <div className="mb-3">
            <label className="form-label">Company symbol</label>
            <select
              className="form-select"
              name="symbol"
              value={formData.symbol}
              onChange={(e) => onChangeHandler(e.target)}
              style={{ display: "block" }}
            >
              {companies.map(company => (
                (company.Symbol && <option key={company.Symbol} value={company.Symbol} label={company["Company Name"]}>{company["Company" +
                " Name"]}</option>)
              ))}
            </select>
          </div>
          <div className="mb-3">
            <label className="form-label">Date interval</label>
            <DatePicker
              className="form-control"
              disabled={loaderStatus}
              selected={startDate}
              onChange={onChangeDate}
              startDate={startDate}
              endDate={endDate}
              selectsRange
              inline
              maxDate={new Date()}
            />
          </div>
          <div className="mb-3">
            <label className="form-label">Email</label>
            <input
              type="email"
              disabled={loaderStatus}
              name="email"
              className="form-control"
              placeholder="Email address"
              onChange={(e) => onChangeHandler(e.target)}
              value={formData.email}
            />
          </div>
          <div>
            { !loaderStatus &&(<button type="submit" className="btn btn-primary">Submit</button>)}
            <ThreeCircles
              height="100"
              width="100"
              color="#4fa94d"
              wrapperStyle={{}}
              wrapperClass=""
              visible={loaderStatus}
              ariaLabel="three-circles-rotating"
            />
          </div>
        </form>
      </div>
      <div className="row">
        {response.length > 0 &&
          <HistoricalQuotes
            prices={response}
          />
        }
      </div>
    </>
  );
}

export default MyForm;

async function symbolValidator(symbol) {
  let response = await fetch("https://pkgstore.datahub.io/core/nasdaq-listings/nasdaq-listed_json/data/a5bc7580d6176d60ac0b2142ca8d7df6/nasdaq-listed_json.json");
  let res = await response.json();
  let searchIndex = await res.findIndex((company) => company.Symbol === symbol.toUpperCase());

  return searchIndex + 1;
} 

export default symbolValidator;